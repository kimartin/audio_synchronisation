import pandas as pd
import numpy as np
import scipy.signal as sgn
import scipy.ndimage as ndimage
import librosa

from numba import njit, prange
import cupy as cp


def find_overlaps(x,
                  start="Start",
                  end="End",
                  separated_by=0.,
                  name='group',
                  by=None,
                  allow_shared_boundaries=True):
    """
    Finds the overlaps in ranges defined by the columns "start" and "end" of pd.DataFrame object x.
    This includes overlaps that include multiple rows at once.

    Example:
    x = pd.DataFrame({"Start": [0, 10, 20, 30, 40, 50, 60],
                      "End":   [9, 21, 28, 53, 42, 55, 70]
    find_overlaps(x)
            Start  End  group
                0    9      1
               10   21      2
               20   28      2
               30   53      3
               40   42      3
               50   55      3
               60   70      4

    :param x: a pd.DataFrame object
    :param start: str. Gives the column that defines the start of the range.
    :param end: str. Gives the column that defines the end of the range.
    :param separated_by: float. Allows extending (if > 0) or shrinking (if <0) intervals so that overlaps will be
    considered either more broadly or more stringently.
        separated_by > 0: overlaps will be considered even if the actual values are separated by up to its value.
            e.g. separated_by = 1: intervals [1, 2.0001] and [2.9999, 4] are STILL considered overlapping.
        separated_by < 0: overlaps will be considered only if the actual values really overlap by more than its value
            e.g. separated_by = -1: intervals [1, 2.9999] and [2.0001, 4] are NOT considered overlapping.
    :param allow_shared_boundaries: bool. If True (default), intervals that share one boundary are not counted as
        overlapping.
    :param by: str or list of str. Optional columns that define groups within x. Rows that overlap in the
        start and end columns, but have different group as defined by this argument, will not be considered overlapping.
    :param name: str. Name of the "group" column
    :return: x with addition "group" column. The values in this column correspond to overlaps.
    """
    # df = x.copy(deep=True)

    # Flatten start and end into a single column
    startdf = pd.DataFrame({'time': x[start] - separated_by / 2, 'what': 1})
    enddf = pd.DataFrame({'time': x[end] + separated_by / 2, 'what': -1})

    # Sort values: "time" for chronological order, and "what" to avoid consider shared boundaries as overlap
    sort_cols = ['time']

    if allow_shared_boundaries:
        sort_cols += ['what']

    if by is not None:
        startdf = pd.concat([startdf, x[by]], axis=1)
        enddf = pd.concat([enddf, x[by]], axis=1)
        sort_cols = [*by, *sort_cols] if isinstance(by, list) else [by, *sort_cols]

    mergdf = pd.concat([startdf, enddf]).sort_values(sort_cols)

    # Count intervals currently going on over the entire column
    mergdf['running'] = mergdf['what'].cumsum()

    # Finds the starts where there is exactly one interval
    mergdf['newwin'] = mergdf['running'].eq(1) & mergdf['what'].eq(1)

    # Group
    mergdf[name] = mergdf['newwin'].cumsum()

    # Adds the group back to the original data
    x[name] = pd.Series(mergdf[name].loc[mergdf['what'].eq(1)], index=x.index)

    x[name] = pd.factorize(x[name])[0]

    return x


def hms_to_s(x, sep=None):
    """
    Converts hms string x to an integer representing the number of seconds since midnight.
    The sep argument is to remove separator strings such as ':'.
    e.g. hms_to_s('08:12:00', sep=':') -> 29520
    """
    if sep is not None:
        x = x.replace(sep, "")
    x = int(x)
    return 3600 * (x // 10000) + 60 * (x % 10000 // 100) + x % 100


def filters(x, pre=None, dc=None, axis=-1):
    """
    Combines premphasis and dc-blocking filters with only one call to scipy.sgn.lfilter

    Both should use values between -1 and 0 (inclusive for pre, exclusive for dc).
    e.g. pre=-1, dc=-.995
    """
    if pre is None and dc is None:
        return x

    b = [1.]
    if pre is not None:
        b += pre if isinstance(pre, list) else [pre]

    a = [1.]
    if dc is not None:
        a += dc if isinstance(dc, list) else [dc]

    return sgn.lfilter(b=b, a=a, x=x, axis=axis)



def frequency_bands(sr, n_fft, band_limits):
    freq = librosa.fft_frequencies(sr=sr, n_fft=n_fft)
    indices = []
    index = 0
    for lim in band_limits:
        index = next((i for i, value in enumerate(freq[index:], start=index) if value >= lim), len(freq))
        indices.append(index)
    return indices


def constellation_map(X, freq_bands=None, dist_freq=0, dist_time=-1, thresh=0.01):
    """
    X should have shape (..., Frequencies, Time)

    """
    assert (freq_bands is not None) ^ (dist_freq > 0), 'Provide one of freq_bands or dist_freq (no more, no less)'

    if freq_bands is not None and dist_time < 0:
        # This is faster, takes less space and functionally equivalent to the second case IF not reducing over time
        Y = [X[..., freq_bands[i]:freq_bands[i + 1], :] for i in range(len(freq_bands) - 1)]
        Y = [np.argmax(_, axis=-2) / _.shape[-2] for _ in Y]
        Y = np.stack(Y, axis=-2)
        return Y

    elif freq_bands is not None:
        Y = [X[..., freq_bands[i]:freq_bands[i+1], :] for i in range(len(freq_bands) - 1)]
        Y = [ndimage.maximum_filter(_, size=[1] * (X.ndim - 2) + [_.shape[-2], 2 * dist_time + 1], mode='constant') for _ in Y]
        Y = np.concatenate(Y, axis=-2)

        X = X[..., freq_bands[0]:freq_bands[-1], :]

    else:
        Y = ndimage.maximum_filter(X, size=[1] * (X.ndim - 2) + [2 * dist_freq + 1, 2 * dist_time + 1], mode='constant')

    return np.logical_and(X == Y, Y > thresh)


# @njit(nogil=True, cache=True)
# def euclidean(a, b):
#     res = np.dot(a.T, b)
#     res *= -2.
#     res += np.expand_dims((a**2).sum(axis=0), axis=-1)
#     res += (b**2).sum(axis=0)
#     return np.abs(res) ** .5
#
#
# def minimum_diagonal(x, min_offset=-np.inf, max_offset=np.inf):
#     min_offset = max(-x.shape[0]+1, min_offset)
#     max_offset = min(max_offset, x.shape[0])
#     arr = np.empty(shape=max_offset - min_offset)
#     for i in range(min_offset, max_offset):
#         arr[i + max_offset - 1] = np.diagonal(x, offset=i).mean()
#     return arr
#
#
# @njit(cache=True, parallel=True)
# def minimum_diagonal2(x, min_offset=np.inf, max_offset=np.inf):
#     rows, cols = x.shape
#     max_offset = int(min(cols, max_offset))
#     min_offset = int(min(rows, min_offset))
#     arr = np.zeros(shape=min_offset + max_offset - 1)
#
#     for i in prange(min_offset + max_offset - 1):
#         diag = []
#         for j in range(max(0, i - max_offset + 1), min(min_offset, i + 1)):
#             diag.append(x[min_offset - j - 1, i - j])
#         arr[i] = sum(diag) / len(diag)
#     return arr


@njit(cache=True, parallel=True)
def match_matrices(x, y, max_lag=np.inf, freq_tol=0.):
    # x and y should have the same dimensions in all but the first axis
    x_cols = len(x)
    y_cols = len(y)
    start = int(max(0, x_cols - max_lag))
    N = x_cols + y_cols - 1
    end = int(min(N, x_cols + max_lag - 1))

    idx = np.arange(start, end)
    lo_x = np.maximum(0, x_cols - 1 - idx)
    up_x = np.minimum(x_cols, N - idx)
    lo_y = np.maximum(0, idx - x_cols + 1)
    up_y = np.minimum(N, idx) + 1

    arr = np.empty(shape=end - start)
    for i in prange(end - start):
        sub_arr = x[lo_x[i]:up_x[i]] - y[lo_y[i]:up_y[i]]
        arr[i] = (np.abs(sub_arr) > freq_tol).sum()

    return arr / (up_x - lo_x)

def get_lag(x, y, max_lag=np.inf, freq_tol=0.):
    lag = match_matrices(x, y, max_lag=max_lag, freq_tol=freq_tol)
    lag = np.argmin(lag) + 1 - min(x.shape[0], max_lag)
    return lag


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    from time import time
    freq_bands = frequency_bands(sr=48000, band_limits=[0, 120, 1000, 2000, 4000, 8000, 16000, 24011], n_fft=960)
    x = np.random.uniform(size=(481, 3000))

    y = constellation_map(x, freq_bands=freq_bands, dist_time=-1)
    y = np.ascontiguousarray(y.T)

    match_matrices(y, y, freq_tol=.1)

    t1 = 0.
    t2 = 0.

    for i in range(100):
        print(i)
        x = np.random.uniform(size=(481, 3000))
        y = constellation_map(x, freq_bands=freq_bands, dist_time=-1)
        y = np.ascontiguousarray(y.T)

        te = time()
        match_matrices(y, y, freq_tol=.1)
        t1 += time() - te

        te = time()
        match_matrices(y, y, freq_tol=.1)
        t2 += time() - te

    print(t1, t2)













