import numpy as np
import os
import re
import pandas as pd
from tqdm import tqdm

import soundfile as sf



if __name__ == '__main__':
    df = pd.read_table('C:/Users/killian/Desktop/lag_sync.tsv', sep='\t')

    for _, group in tqdm(df.groupby(['day', 'group']), total=df.groupby(['day', 'group']).ngroups):
        frames = int((group['frames'] + group['lag']).max())
        channels = group.drop_duplicates(subset=['indicator'], keep='first')['channels'].sum()

        synchronised = np.zeros(shape=(frames, channels), dtype=np.int16)
        dummy = np.zeros_like(synchronised)
        for file, indicator, lag, frames in group[['name', 'indicator', 'lag', 'frames']].itertuples(index=False):
            if indicator in ['SM4A', 'SM4CAMA']:
                channels = [0, 1]
            elif indicator in ['SM4B', 'SM4CAMB']:
                channels = [2, 3]
            else:
                channels = [4, 5]
            channels = np.array(channels)

            data, _ = sf.read(file=file, always_2d=True, dtype=np.int16)
            synchronised[lag:(lag+frames), channels] = data
            dummy[lag:(lag + frames), channels] += 1

        print(np.max(dummy), np.sum(dummy), sum([sf.info(file).frames * sf.info(file).channels for file in group['name']]))

        if np.max(dummy) == 1 and np.sum(dummy) == sum([sf.info(file).frames * sf.info(file).channels for file in group['name']]):
            print('Writing')
            sf.write('C:/Users/killian/Desktop/test.flac', data=synchronised, samplerate=48000, format='FLAC')

        break

