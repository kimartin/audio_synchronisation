import numpy as np
import scipy.signal as sgn

import os
import re
import pandas as pd
from tqdm import tqdm

import soundfile as sf

from utilities import find_overlaps, filters, frequency_bands, constellation_map, get_lag


def get_minimal_pairs_for_sync(x, start_col='start_s', end_col='end_s'):
    idx_pairs = []
    for i in range(x.shape[0]):
        for j in range(i + 1, x.shape[0]):
            new_group = find_overlaps(group.iloc[[i, j]].reset_index(drop=True), start=start_col, end=end_col)
            if new_group['group'].nunique() == 1:
                idx_pairs.append(((i, j), new_group[end_col].min() - new_group[start_col].max()))

    unique_values = {x for l in idx_pairs for x in l[0]}
    idx_pairs = sorted(idx_pairs, key=lambda x: x[-1], reverse=True)
    initial_pair = idx_pairs.pop(0)
    out_pairs = [initial_pair[0]]
    already_in = {x for l in out_pairs for x in l}
    while already_in != unique_values:
        # find the pair that has exactly one element not in the unique values of out_pairs
        candidates = [i for i, (pair, ovlp) in enumerate(idx_pairs) if len(set(pair).difference(already_in)) == 1]
        # take the longest overlap
        if len(candidates) > 0:
            out_pairs.append(idx_pairs.pop(candidates[0])[0])
        already_in = {x for l in out_pairs for x in l}
    return out_pairs


if __name__ == '__main__':
    max_overlap_length_in_seconds = 300
    nperseg = 480
    noverlap = 384
    nfft = 960
    window = "hamming"
    power = 2
    dB_threshold = 20
    band_limits = [120, 1000, 2000, 4000, 10000]
    threshold = 0.01

    df = pd.read_table('C:/Users/killian/Desktop/sync.tsv', sep='\t')

    for _, group in tqdm(df.groupby(['day', 'group']), total=df.groupby(['day', 'group']).ngroups):
        if group['indicator'].nunique() == group.shape[0] or group.shape[0] < 4:
        # if group['indicator'].nunique() != group.shape[0] or group.shape[0] <= 2:
            continue

        if group.shape[0] == 1:
            new_df = group.assign(lag=0, lag_s=0 / 48000)
            new_df.to_csv('C:/Users/killian/Desktop/lag_sync.tsv', sep='\t', index=False, mode='a',
                          header=not os.path.exists('C:/Users/killian/Desktop/lag_sync.tsv'))
            continue

        lag_all = []
        minimal_pairs = get_minimal_pairs_for_sync(group)
        pd.options.display.width = 0
        for i, j in minimal_pairs:
            new_group = group.iloc[[i, j]].copy()
            new_group['overlap_start'] = new_group['start_s'].max() - new_group['start_s']
            new_group['overlap_length'] = new_group['end_s'].min() - new_group['start_s'].max()
            new_group['overlap_duration'] = np.minimum(min(max_overlap_length_in_seconds,
                                                           new_group['end_s'].min() - new_group['start_s'].max()),
                                                       new_group['duration'] - new_group['overlap_start'])
            print(new_group)

            # Read data, keeping only the first channel
            for duration_coef in [.1, .2, .25, .5, .75, .9, 1.]:
                data = [sf.read(file=file,
                                start=int(start * sr),
                                frames=int(duration * sr * duration_coef),
                                always_2d=True)[0]
                        for file, start, duration, sr in
                        new_group[['name', 'overlap_start', 'overlap_duration', 'samplerate']].itertuples(index=False)]
                data = [np.ascontiguousarray(_[:, :1].T) for _ in data]

                longest = max([_.shape[0] for _ in data])
                data = [np.pad(_, pad_width=[(0, longest - _.shape[0]), (0, 0)], constant_values=0) for _ in data]
                data = np.concatenate(data, axis=0)

                # Compute any necessary transform
                data = filters(data, pre=-1, axis=-1)

                # Compute spectrograms
                spec = sgn.stft(data, nperseg=nperseg, noverlap=noverlap, nfft=int(nperseg * 2), padded=True, window=window)[-1]
                spec = np.abs(spec) ** power

                with np.errstate(invalid='ignore', divide='ignore'):
                    spec = np.log10(spec / spec.max(axis=(1, 2), keepdims=True))
                spec = np.maximum(spec + dB_threshold, 0)

                # Compute constellation map
                C = constellation_map(spec,
                                      freq_bands=frequency_bands(sr=new_group['samplerate'].iloc[0],
                                                                 n_fft=int(2 * nperseg),
                                                                 band_limits=band_limits),
                                      dist_time=-1,
                                      thresh=threshold)
                C = np.ascontiguousarray(np.moveaxis(C, source=2, destination=1))

                print((i, j), duration_coef, C.shape[1], get_lag(C[1], C[0], max_lag=C.shape[1] // 2))

        #     lag_all.append((i, j, get_lag(C[1], C[0], max_lag=C.shape[1] // 2)))
        #
        # final_lags = np.full(shape=group.shape[0], fill_value=np.inf)
        # final_lags[lag_all[0][0]] = 0
        # while len(lag_all) > 0:
        #     idx, file1, file2, lag = [(idx, i, j, l) for idx, (i, j, l) in enumerate(lag_all) if (final_lags[i] == np.inf) ^ (final_lags[j] == np.inf)][0]
        #     lag_all.pop(idx)
        #     if final_lags[file1] != np.inf and final_lags[file2] == np.inf:
        #         final_lags[file2] = final_lags[file1] + lag
        #     elif final_lags[file1] == np.inf and final_lags[file2] != np.inf:
        #         final_lags[file1] = final_lags[file2] - lag
        #
        # # final_lags = -final_lags
        # final_lags -= final_lags.min()
        # final_lags *= (nperseg - noverlap)
        #
        # final_lags = final_lags + (group['start_s'] * group['samplerate']) - (group['start_s'] * group['samplerate']).min()
        #
        # new_df = group.assign(lag=final_lags.astype('int'), lag_s=final_lags / 48000)
        # new_df.to_csv('C:/Users/killian/Desktop/lag_sync.tsv', sep='\t', index=False, mode='a',
        #               header=not os.path.exists('C:/Users/killian/Desktop/lag_sync.tsv'))
