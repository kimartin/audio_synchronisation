import os
import pandas as pd
from tqdm import tqdm
import soundfile as sf

from utilities import hms_to_s, find_overlaps


def prepare_df(filelist, indicator_pattern, time_pattern, day_pattern, path=None):
    if path is not None:
        filelist = [f'{path}/{_}' for _ in filelist]

    df = []
    attributes = ['name', 'channels', 'frames', 'duration', 'samplerate']
    for file in tqdm(filelist, total=len(filelist)):
        df_ = sf.info(file, verbose=True)
        df_ = pd.DataFrame({_: getattr(df_, _) for _ in attributes}, index=[0])
        df.append(df_)
    df = pd.concat(df, ignore_index=True, axis=0)

    df['file'] = df['name'].str.replace(f'{path}/', '')
    df['indicator'] = df['name'].str.extract(f'({indicator_pattern})', expand=True)
    df['loc'] = ['CAMB' if 'CAM' in _ else 'STRA' for _ in df['file']]
    df['day'] = df['name'].str.extract(f'({day_pattern})', expand=True)
    df['time'] = df['name'].str.extract(f'({time_pattern})', expand=True)
    df['start_s'] = df['time'].apply(hms_to_s)
    df['end_s'] = df['start_s'] + df['duration']

    df = df.sort_values(by=['day', 'indicator', 'start_s']).reset_index(drop=True)

    df = find_overlaps(df, start='start_s', end='end_s', name='group', by=['loc', 'day'])

    filenames = df.groupby(['group']).agg({'loc': 'first', 'day': 'first', 'time': 'min'})
    filenames = filenames.apply(lambda x: '{}_{}_{}.flac'.format(*x), axis=1)
    df['filename'] = df['group'].map(filenames)
    return df


if __name__ == '__main__':
    paths = [
        "F:/Database_Backup/data/2020_Killian/Rooks/raw/audio/strasbourg_aviary",
        "F:/Database_Backup/data/2020_Killian/Rooks/raw/audio/cambridge_aviary",
        "F:/Database_Backup/data/2020_Killian/Jays/raw/audio"
    ]

    df = [prepare_df(filelist=[f for f in os.listdir(in_path) if f.endswith('flac') or f.endswith('wav')],
                    indicator_pattern=r"SM4[A-Z]+",
                    time_pattern=r"(?<=[^\d])\d{6}(?=[^\d])",
                    day_pattern=r"(?<=[^\d])\d{8}(?=[^\d])",
                    path=in_path)
           for in_path in paths]

    df = pd.concat(df, axis=0, ignore_index=True)
    df.to_csv('C:/Users/killian/Desktop/sync.tsv', sep='\t', index=False)